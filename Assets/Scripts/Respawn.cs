﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    [SerializeField] GameObject PortalRedToMap1;
    [SerializeField] GameObject PortalRedToMap2;
  public void respawn()
  {
      transform.position =new Vector3(2.36f,66,118);
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
  public void respawn0()
  {
      transform.position =new Vector3(0,2,0);
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
  public void respawn3()
  {
      transform.position =new Vector3(-10,56,277);
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
  public void PortalRed1()
  {
      transform.position =new Vector3(-17.59f,67.21f,392.69f);
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
  public void PortalRedMap1()
  {
      transform.position = PortalRedToMap1.transform.position;
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
  public void PortalRedMap2()
  {
      transform.position = PortalRedToMap2.transform.position;
      transform.rotation=Quaternion.EulerAngles(0,0,0);
  }
}
